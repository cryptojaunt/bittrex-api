const http = require('./http');

class Account{

  /**
   * Create a Bittrex Account API client
   * @param {String} key - API Key
   * @param {String} secret - API Secret
   */
  constructor(key, secret){
    if(!key || key == '' || typeof key !== 'string'){
      throw new Error('An API key is required to access the account API');
    }
    else if(!secret || secret == '' || typeof secret !== 'string'){
      throw new Error('An API secret is required to access the account API');
    }

    this._key = key;
    this._secret = secret;
  }

  /**
   * Get all account balances
   * @returns {Promise} - API response
   */
  getBalances(){
    return this._makeRequest('getbalances');
  }

  /**
   * Get account balance for specified currency
   * @param {String} currency - Currency ex BTC
   * @returns {Promise} - API response
   */
  getBalance(currency = ''){
    return this._makeRequest('getbalance', [{name: 'currency', value: currency}]);
  }

  /**
   * Get deposit address for specified currency
   * @param {String} currency - Currency to withdraw ex BTC
   * @returns {Promise} - API response
   */
  getDepositAddress(currency = ''){
    return this._makeRequest('getdepositaddress', [{name: 'currency', value: currency}]);
  }

  /**
   * Withdraw for a given currency
   * @param {String} currency - Currency to withdraw ex BTC
   * @param {(Float|Integer|String)} quantity - Quantity of coins to send ex 0.01
   * @param {String} address - Address to send payment to ex Xq13ASq.... 
   * @param {String} [paymentId] - Optional used for CryptoNotes/BitShareX/Nxt optional field (memo/paymentid)
   * @returns {Promise} - API response
   */
  withdraw(currency = '', quantity = '', address = '', paymentId = ''){
    return this._makeRequest('withdraw', [
      {name: 'currency', value: currency},
      {name: 'quantity', value: quantity},
      {name: 'address', value: address},
      {name: 'paymentid', value: paymentId}
    ]);
  }

  /**
   * Get an order status
   * @param {String} uuid - Unique ID for an order
   * @returns {Promise} - API response
   */
  getOrder(uuid = ''){
    return this._makeRequest('getorder', [{name: 'uuid', value: uuid}]);
  }

  /**
   * Get order history
   * @param {String} [market] - Market ex BTC-LTC. If excluded will return for all markets
   * @returns {Promise} - API response
   */
  getOrderHistory(market = ''){
    return this._makeRequest('getorderhistory', [{name: 'market', value: market}]);
  }

  /**
   * Get withdrawal history
   * @param {String} [currency] - Currency ex BTC. If excluded will return for all currencies
   * @returns {Promise} - API response
   */
  getWithdrawalHistory(currency = ''){
    return this._makeRequest('getwithdrawalhistory', [{name: 'currency', value: currency}]);
  }

  /**
   * Get deposit history
   * @param {String} [currency] - Currency ex BTC. If excluded will return for all currencies
   * @returns {Promise} - API response
   */
  getDepositHistory(currency = ''){
    return this._makeRequest('getdeposithistory', [{name: 'currency', value: currency}]);
  }

  /**
   * Make an API request
   * @param {String} uri - Request URI fragment
   * @param {Object<name: String, value: String>[]} parameters  - Request parameters
   * @returns {Promise} - API response
   */
  _makeRequest(uri, params){
    return http.makeRequest(`account/${uri}`, parameters, this._key, this._secret);
  }

}

module.exports = Account;