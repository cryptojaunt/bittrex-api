const crypto = require('crypto');
const fetch = require('node-fetch');

class Http{

  /**
   * Make an API request
   * @param {String} uri - Request URI
   * @param {Array[Object<name: String, value: String>]} [parameters] - Request parameters
   * @param {String} key - API key
   * @param {String} secret - API secret
   * @returns {Promise} - API response
   */
  static async makeRequest(uri, parameters, key = '', secret = ''){
    let err;
    let json;

    if(uri.includes('public')){
      json = await this._makePublicRequest(uri, parameters)
        .catch(e => err = e);
    }
    else{
      json = await this._makeAuthRequest(uri, parameters, key, secret)
        .catch(e => err = e);
    }
    
    if(err)
      return Promise.reject(err);

    return this._processResponse(json);
  }

  /**
   * Make a public API request
   * @param {String} url - Request URI
   * @param {Array[Object<name: String, value: String>]} parameters  - Request parameters
   * @returns {Promise} - API response
   */ 
  static async _makePublicRequest(url, parameters){
    const uri = this._makeUri(url, parameters);

    const res = await fetch(uri);

    return res.json();

  }

  /**
   * Make an authenticated API request
   * @param {String} uri - Reuest URI
   * @param {Array[Object<name: String, value: String>]} parameters - Request parameters
   * @param {String} key - API key
   * @param {String} secret - API secret
   * @returns {Object} - API request response
   */
  static async _makeAuthRequest(uri, parameters, key, secret){
    const url = this._makeUri(uri, parameters, key);

    console.log(url);

    const header = this._makeAuthHeader(url, secret);

    const res = await fetch(uri, {headers: header});

    return res.json();
  }

  /**
   * Handle JSON response
   * @param {Object} json - API request response
   * @returns {Promise} - API response
   */
  static _processResponse(json){
    let val;

    if(json.success){
        val = Promise.resolve(json.result);
    }
    else{
        val = Promise.reject(json.message);
    }

    return val;

  }
    
  /**
   * Make a URI for the provided API endpoint 
   * @param {String} url - Request URL
   * @param {Object<name: String,value: String>[]} parameters - Request parameters
   * @param {String} key - API key
   * @returns {String} - Request URI
   */
  static _makeUri(url, parameters, key = ''){
    let uri = `https://bittrex.com/api/v1.1/${url}?`;
    
    if(!url.includes('public')){
      const nonce =  this._getNonce();

      uri += `apikey=${key}&nonce=${nonce}`;
    }
    
    if(parameters && Array.isArray(parameters))
      parameters.map(param => {
        uri += `&${param.name}=${param.value}`;
      });
    
    return uri;

  }

  /**
   * Create a nonce for the API request
   * @returns {Integer} - Nonce for API request
   */
  static _getNonce(){
    return Math.round((new Date()).getTime() / 1000);
  }

  /**
   * Make a signed auth header
   * @param {String} uri - Request URI
   * @param {String} secret - API secret
   * @returns {Object<apisign: String>} - Header object
   */
  static _makeAuthHeader(uri, secret){
    const signed = crypto.createHmac('sha512', secret)
      .update(uri)
      .digest('hex');
    
    return {'apisign': signed};

  }

}

module.exports = Http;