const http = require('./http');

class Market{
  
  /**
   * Create a Bittrex Market API client
   * @param {String} key - API key
   * @param {String} secret  - API secret
   */
  constructor(key, secret){
    if(!key || key == '' || typeof key !== 'string'){
      throw new Error('An API key is required to access the market API');
    }
    else if(!secret || secret == '' || typeof secret !== 'string'){
      throw new Error('An API secret is required to access the market API');
    }

    this._key = key;
    this._secret = secret;
  }

  /**
   * Create a limit-buy order
   * @param {String} market - Market to trade ex BTC-LTC
   * @param {String} quantity - Amount to purchase
   * @param {String} rate - Rate at which to execute the order
   * @returns {Promise} - API response
   */
  async buyLimit(market = '', quantity = '', rate = ''){
    return this._makeRequest('buylimit', [
      {name: 'market', value: market},
      {name: 'quantity', value: quantity},
      {name: 'rate', value: rate}
    ]);
  }

  /**
   * Create a limit-sell order
   * @param {String} market - Market to trade ex BTC-LTC
   * @param {Float|Integer|String} quantity - Amount to sell
   * @param {Float|Integer|String} rate - Rate at which to execute the order
   * @returns {Promise} - API response
   */
  async sellLimit(market = '', quantity = '', rate = ''){
    return this._makeRequest('selllimit', [
      {name: 'market', value: market},
      {name: 'quantity', value: quantity},
      {name: 'rate', value: rate}
    ]);
  }

  /**
   * Cancel an order
   * @param {String} uuid - Unique ID of the order
   * @returns {Promise} - API response
   */
  async cancelOrder(uuid = ''){
    return this._makeRequest('cancel', [{name: 'uuid', value: uuid}]);
  }

  /**
   * Get open orders
   * @param {String} market - Market to trade ex BTC-LTC
   * @returns {Promise} - API response
   */
  async getOpenOrders(market = ''){
    return this._makeRequest('getopenorders', [{name: 'market', value: market}]); 
  }

  /**
   * Make an API request
   * @param {String} uri - Request URI fragment
   * @param {Object<name: String, value: String>[]} parameters - Request parameters
   * @returns {Promise} - API response
   */
  _makeRequest(uri, parameters){
    return http.makeRequest(`market/${uri}`, parameters, this._key, this._secret);
  }

}

module.exports = Market;