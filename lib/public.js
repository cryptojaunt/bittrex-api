const http = require('./http');

class Public{
  /**
   * Get open and available trading markets along with other meta data
   * @returns {Promise} - API response
   */
  async getMarkets(){
    return this._makeRequest('getmarkets');
  }

  /**
   * Get all supported currencies and other meta data
   * @returns {Promise} - API response
   */
  async getCurrencies(){
    return this._makeRequest('getcurrencies');
  }

  /**
  * Get the current ticker values for a market
  * @param {String} market - Market ex BTC-LTC
  * @returns {Promise} - API response
  */
  async getTicker(market = ''){
    return this._makeRequest('getticker', [{name: 'market', value: market}]);
  }

  /**
   * Get the last 24 hour summary of all active exchanges
   * @returns {Promise} - API response
   */
  async getMarketSummaries(){
    return this._makeRequest('getmarketsummaries');
  }

  /**
   * Get the last 24 hour summary of all active exchanges
   * @param {String} market - Market ex BTC-LTC
   * @returns {Promise} - API response
   */
  async getMarketSummary(market = ''){
    return this._makeRequest('getmarketsummary', [{name: 'market', value: market}])
  }

  /**
   * Retrieve the orderbook for a given market
   * @param {String} market - Market ex BTC-LTC
   * @param {String} type [type=both] - Type of order: buy, sell or both
   * @returns {Promise} - API response
   */
  async getOrderBook(market = '', type = 'both'){
    return this._makeRequest('getorderbook', [{name: 'market', value: market}, {type: type}]);
  }

  /**
   * Retrieve the latest trades that have occured for a specific market.
   * @param {String} market - Market ex BTC-LTC
   * @returns {Promise} - API response
   */
  async getMarketHistory(market = ''){
    return this._makeRequest('getmarkethistory', [{name: 'market', value: market}]);
  }

  /**
   * Make a request and parse the response
   * @param {String} url - URI fragment
   * @param {String} parameters - Request parameters
   * @returns {Promise} - API response
   */
  async _makeRequest(url, parameters){
    return http.makeRequest(`public/${url}`, parameters)
  }
}

module.exports = Public;