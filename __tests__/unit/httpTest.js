const httpPath = '../../lib/http';
const crypto = require('crypto');

jest.mock('node-fetch', () => Promise.resolve({
    json: jest.fn(() => Promise.resolve({}))
}));

test('http is defined', () => {
    expect(require(httpPath)).toBeDefined();
});

test('_makeAuthHeader creates an object w a signed value', () => {
    const http = require(httpPath);

    const uri = 'test';
    const secret = 'secret';

    const signed = crypto.createHmac('sha512', secret)
        .update(uri)
        .digest('hex');
    
    const authHeader = http._makeAuthHeader(uri, secret);

    expect(authHeader).toBeDefined();

    expect(authHeader.apisign).toBeDefined();

    expect(authHeader.apisign).toEqual(signed);

});

test('_getNonce returns a number', () => {
    const http = require(httpPath);

    const nonce = http._getNonce();

    expect(nonce).toBeDefined();

    expect(typeof nonce).toEqual('number');

});

test('_getNonce returns a timestamp', () => {
    const http = require(httpPath);

    const nonce = http._getNonce();

    const today = new Date();
    const date = new Date();

    date.setSeconds(nonce);

    expect(today.getDay).toEqual(date.getDay);
    expect(today.getDate).toEqual(date.getDate);
    expect(today.getMonth).toEqual(date.getMonth);
    expect(today.getFullYear).toEqual(date.getFullYear);
    expect(today.getHours).toEqual(date.getHours);
    expect(today.getMinutes).toEqual(date.getMinutes);

});

test('_makeUri returns a formatted URI', () => {
    const http = require(httpPath);

    const uri = http._makeUri('public/test');

    expect(uri).toBeDefined();

    expect(uri).toEqual('https://bittrex.com/api/v1.1/public/test?');

});

test('_makeUri appends parameters', () => {
    const http = require(httpPath);

    const uri = http._makeUri('public/test', [{name: 'test', value: '123'}]);

    expect(uri).toBeDefined();

    expect(uri).toEqual('https://bittrex.com/api/v1.1/public/test?&test=123');

});

test('_makeUri appends api key and nonce for non-public URI', () => {
    const http = require(httpPath);

    http._getNonce = () => 1;

    const uri = http._makeUri('test', null, 'test');

    expect(uri).toBeDefined();

    expect(uri).toEqual('https://bittrex.com/api/v1.1/test?apikey=test&nonce=1');

});

test('_processResponse resolves if JSON has success', () => {
    const http = require(httpPath);

    http._processResponse({success: true, result: [{}]})
        .then(data => {
            expect(data).toBeDefined()

            expect(data).toEqual([{}]);
        });
});

test('_processResponse rejects if !JSON.success', async () => {
    const http = require(httpPath);

    const cb = jest.fn(e => {
        expect(e).toBeDefined();
        expect(e).toEqual('error');
    });

    await http._processResponse({success: false, result: [{}], message: 'error'})
        .catch(cb);

    expect(cb).toBeCalled();

});

test('_makeAuthRequest returns request result', async () => {
    const http = require(httpPath);

    const res = await http._makeAuthRequest('test', null, 'ok', 'ok');

    expect(fetch).toBeCalled();
    expect(jsonResolve).toBeCalled();

    expect(res).toEqual({});

});