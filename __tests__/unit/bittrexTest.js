test('bittrex.js exports the Bittrex client object', () => {
    const Bittrex = require('../../lib/bittrex');

    expect(Bittrex).toBeDefined();

    expect(Bittrex.Account).toBeDefined();

    expect(Bittrex.Market).toBeDefined();

    expect(Bittrex.Public).toBeDefined();

});