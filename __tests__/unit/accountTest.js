const AccountAPIClient = require('../../lib/account');

test('Instantiating an Account API client without API key should throw an error', () => {
    expect(() => {
        new AccountAPIClient()
    }).toThrow()

    expect(() => {
        new AccountAPIClient('')
    }).toThrow()

});

test('Instantiating an Acocunt API client with invalid API key should throw an error', () => {
    expect(() => {
        new AccountAPIClient({})
    }).toThrow()

    expect(() => {
        new AccountAPIClient([])
    }).toThrow()

    expect(() => {
        new AccountAPIClient(123)
    }).toThrow()

    expect(() => {
        new AccountAPIClient(() => {})
    }).toThrow()
});

test('Instantiating an Account API client without API secret should throw an error', () => {
    expect(() => {
        new AccountAPIClient('a')
    }).toThrow()

    expect(() => {
        new AccountAPIClient('a', '')
    }).toThrow()

});

test('Instantiating an Account API client with invalid API secret should throw an error', () => {
    expect(() => {
        new AccountAPIClient('a', {})
    }).toThrow()

    expect(() => {
        new AccountAPIClient('a', [])
    }).toThrow()

    expect(() => {
        new AccountAPIClient('a', 123)
    }).toThrow()

    expect(() => {
        new AccountAPIClient('a', () => {})
    }).toThrow()
});
    
    