const MarketAPIClient = require('../../lib/market');

test('Instantiating a Market API client without API key should throw an error', () => {
    expect(() => {
        new MarketAPIClient()
    }).toThrow()

    expect(() => {
        new MarketAPIClient('')
    }).toThrow()

});

test('Instantiating a Market API client with invalid API key should throw an error', () => {
    expect(() => {
        new MarketAPIClient({})
    }).toThrow()

    expect(() => {
        new MarketAPIClient([])
    }).toThrow()

    expect(() => {
        new MarketAPIClient(123)
    }).toThrow()

    expect(() => {
        new MarketAPIClient(() => {})
    }).toThrow()
});

test('Instantiating a Market API client without API secret should throw an error', () => {
    expect(() => {
        new MarketAPIClient('a')
    }).toThrow()

    expect(() => {
        new MarketAPIClient('a', '')
    }).toThrow()

});

test('Instantiating a Market API client with invalid API secret should throw an error', () => {
    expect(() => {
        new MarketAPIClient('a', {})
    }).toThrow()

    expect(() => {
        new MarketAPIClient('a', [])
    }).toThrow()

    expect(() => {
        new MarketAPIClient('a', 123)
    }).toThrow()

    expect(() => {
        new MarketAPIClient('a', () => {})
    }).toThrow()
});
    
    