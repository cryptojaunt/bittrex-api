const publicAPIClient = require('../../lib/public');

test('Public API client should instantiate successfully', () => {
    expect(new publicAPIClient()).toBeDefined();
})