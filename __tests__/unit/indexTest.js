test('index.js exports the Bittrex client object', () => {
    const Bittrex = require('../../index');

    expect(Bittrex).toBeDefined();

    expect(Bittrex.Account).toBeDefined();

    expect(Bittrex.Market).toBeDefined();

    expect(Bittrex.Public).toBeDefined();

});